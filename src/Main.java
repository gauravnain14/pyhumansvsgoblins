import java.util.Scanner;
public class Main {
    /*
    Use UTF characters for the players and goblins and the land
    Game is turn based move: n/s/e/w
    Once a human and goblin collide combat is initiated
    Combat uses math.random
 */
    public static final int gameSize = 10; // 4x4 game world
    public static final int hp = 100; // 100 hp for both players
    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        Land land = new Land(gameSize);
        Humans human = new Humans(0, hp);
        Goblins goblin = new Goblins( gameSize-1, hp);
        land.initializeGameWorld(human, goblin);
        System.out.println(land.toString());
        boolean intersect = false;
        while(!intersect){
            System.out.println("Make your turn: North(n), South(s), East(e) or West(w)");
            String turn = scanner.next();
            human = land.moveHuman(human, turn.charAt(0));
            goblin = land.moveGoblin(goblin);
            System.out.println(land.toString());
            intersect = land.intersect(human, goblin);
        }
        battleMethod(human, goblin);
    }

    public static void battleMethod(Humans human, Goblins goblin){
        int evenOdd = 0;
        System.out.println("Battle begins! Both Human and Goblin have " + hp + " HP");
        do{
            int random = (int)(Math.random()*100+1);
            if(evenOdd % 2 == 0){
                goblin.setHp(goblin.getHp()-random); //Human damages goblin
                System.out.println("Human does " + random + " damage! Goblin's HP: " + goblin.getHp());

            }else{
                human.setHp(human.getHp()-random); //Goblin damages human
                System.out.println("Goblin does " + random + " damage! Human's HP: " + human.getHp());
            }
            evenOdd++;
        }while(human.getHp() > 0 && goblin.getHp() > 0);
        if(human.getHp() < 1)
            System.out.println("Better luck next time, human!");
        else
            System.out.println("Better luck next time, goblin!");
    }
}