public class Land {
    public String[][] gameWorld;
    public Land(int size){
        gameWorld = new String[size][size];
    }

    public void initializeGameWorld(Humans human, Goblins goblin){
        for(int i = 0; i < gameWorld.length; i++)
            for(int j = 0; j < gameWorld[0].length; j++)
                if(i == human.getX() && j == human.getY())
                    gameWorld[i][j] = "H";
                else if(i == goblin.getX() && j == goblin.getY())
                    gameWorld[i][j] = "G";
                else
                    gameWorld[i][j] = "_";
    }

    public Humans moveHuman(Humans human, char dir){
        int row = human.getX();
        int col = human.getY();
        int size = gameWorld.length-1;
        if((dir=='n'&& col>0) || (dir=='s'&&col<size) || (dir=='w'&&row>0) || (dir=='e'&&row<size)) {
            gameWorld[col][row] = "_";
            switch (dir) {
                case 'n': human.setY(--col); break;
                case 's': human.setY(++col); break;
                case 'w': human.setX(--row); break;
                default: human.setX(++row); break;
            }
            gameWorld[col][row] = "H";
        }else
            System.out.println("Invalid move");
        return human;
    }

    public Goblins moveGoblin(Goblins goblin){
        int row = goblin.getX();
        int col = goblin.getY();
        int size = gameWorld.length-1;
        boolean setPos = false;
        do {
            gameWorld[col][row] = "_";
            switch ((int)(Math.random()*3)) {
                case 0: if(col>0){goblin.setY(--col); setPos=true;} break;
                case 1: if(col<size) {goblin.setY(++col); setPos=true;} break;
                case 2: if(row>0) {goblin.setX(--row); setPos=true;} break;
                default: if(row<size) {goblin.setX(++row); setPos=true;} break;
            }
        }while(!setPos);
        gameWorld[col][row] = "G";
        return goblin;
    }

    public boolean intersect(Humans humans, Goblins goblin){
        int hX = humans.getX();
        int hY = humans.getY();
        int gX = goblin.getX();
        int gY = goblin.getY();
        return (hX==gX && hY==gY) || // overlap
                (hX==(gX-1) && hY==gY) || (hX==(gX+1) && hY==gY) || // human west and east
                (hX==gX && hY==(gY-1)) || (hX==gX && hY==(gY+1)) || // human north and south
                (gX==(hX-1) && gY==hY) || (gX==(hX+1) && gY==hY) || // goblin west and east
                (gX==hX && gY==(hY-1)) || (gX==hX && gY==(hY+1)); // goblin north and south
    }

    public String toString(){
        String game = "";
        for (String[] strings : gameWorld) {
            for (int j = 0; j < gameWorld[0].length; j++)
                game = game.concat(strings[j] + " ");
            game = game.concat("\n");
        }
        return game;
    }
}