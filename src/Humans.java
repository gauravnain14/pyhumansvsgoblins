public class Humans {
    private int x, y, hp;
    public Humans(int size, int hp){
        this.x = size;
        this.y = size;
        this.hp = hp;
    }
    public int getX(){ return x; }
    public int getY(){ return y; }
    public int getHp(){ return hp; }
    public void setX(int x){ this.x = x; }
    public void setY(int y){ this.y = y; }
    public void setHp(int hp){ this.hp = hp; }
    public String toString(){
        return super.toString();
    }
}
